# /bin/bash

#Parece que la virgula ~ no funciona, en vez de eso hay que usar $HOME
yeardir="$HOME/Escritorio/2022"
# para proximas versiones:
# detectar el entorno e instalar:
# screenkey
if test -d $yeardir; then
	echo "$yeardir existe, mejor no seguimos con el proceso.\n"
else
	echo "procedo a crear la carpeta\n"
	mkdir $yeardir
	echo "Nos situamos en la carpeta donde van los repos\n"
	cd $yeardir
	echo "creamos los repositorios. La contraseña de git abra que actualizarla" 
	git clone https://gitlab.com/martin.aranda.ricardo/archivosconfig.git
	git clone https://github.com/Erick31416/Erick31416.github.io.git 
	git clone https://Erick31416:password@github.com/Erick31416/ApuntesPrivado.git
	# donde pone passport tal vez deberia poner token.
	# si clonas el repositorio de esta manera, luego tendras que tenovar el token cuando caduque.
	# para renobar el token caducado :
	# git remote set-url https://Erick31416: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@github.com/Erick31416/ApuntesPrivado.git

	cd "$HOME"

	if [ -f $HOME/.vimrc ] || [ -L $HOME/.vimrc ]; then
		mv $HOME/.vimrc $HOME/.vimrc_$fecha
	fi
	ln -s $yeardir/archivosconfig/vimrc.20220503 $HOME/.vimrc

	if [ -f $HOME/.config/nvim/init.vim ] || [ -L $HOME/.config/nvim/init.vim ]; then
		mv $HOME/.config/nvim/init.vim $HOME/.config/nvim/init.vim_$fecha
	fi
	ln -s $yeardir/archivosconfig/init.vim $HOME/.config/nvim/init.vim

	if [ -f $HOME/.bash_aliases ] || [ -L $HOME/.bash_aliases ]; then
		mv $HOME/.bash_aliases $HOME/.bash_aliases_$fecha
	fi
	ln -s $yeardir/archivosconfig/bash_aliases.20220503 $HOME/.bash_aliases

	if [ -f $HOME/arranca.sh ] || [ -L $HOME/arranca.sh ]; then
		mv $HOME/arranca.sh $HOME/arranca.sh_$fecha
	fi
	ln -s $yeardir/archivosconfig/arranca.sh $HOME/arranca.sh

	if [ -f $HOME/.gitconfig ] || [ -L $HOME/.gitconfig ]; then
		mv $HOME/.gitconfig $HOME/.gitconfig_$fecha
	fi
	ln -s $yeardir/archivosconfig/gitcofig.2022100918 $HOME/.gitconfig
	
fi


