get-date -uformat "%V"

write-output "Este archivo esta en: $profile.CurrentUserAllhost"
write-output 'recuerda que este archivo lo puedes abrir escribiendo code $profile.CurrentUserAllhost'


function conecta-raspBerry {
    ssh pi@192.168.1.40
}

function dame-ip-raspBerry {
    write-output "192.168.1.40"
}

function cargador {
    ~/cargador.ps1
}

function pro {
    code $profile.CurrentUserAllhost
}
function ir-apuntes{
    Set-Location ~/desktop/2020/ApuntesPrivado
}

function preguntaRespuesta ($p, $s){
    
    do{
        $userr = read-host $p
        $sw_reintentar = $false
        if (! ($userr -eq $s)){
            $user_reintentar = read-host "again?"
            if ($user_reintentar -eq "s"){
                $sw_reintentar = $true
            }
        }
    }while ($sw_reintentar)
    write-output "la respuesta era: $s "
}

function hacerPreguntas {
    $dow = date -uformat "%A"
    if ($dow -eq "domingo"){
        write-output "preguntas del $dow"
        preguntaRespuesta "que significa cdn" "content distribution net"
        preguntaRespuesta "El gestor de paqueteria yum es de ...." "centos"
    }
    preguntaRespuesta "puerto para Oracle" "Oracle:1521"
    $part1 = "'$dow' esta en la variable "+'$dow'+" y queremos mostrar 'hoy es $dow' <?Debemos usar comillas dobles o simples?"
    preguntaRespuesta $part1 "dobles"
    preguntaRespuesta "como hacer un enlace simbolico en windows de c:/paco.txt a c:/enlaces/" "new-item -itemtype symboliclink c:/enlaces/paco.txt -value c:/paco.txt"
}
